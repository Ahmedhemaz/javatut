package com.company;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        printArray(sortDec(getIntegers(5)));
    }


    public static int[] getIntegers (int capacity) {
        int[] myArray = new int[capacity] ;
        for(int i=0; i<myArray.length;i++) {
            myArray[i] = scanner.nextInt();
        }
        return myArray;
    }


    public static int[] sortDec(int[] array) {

        boolean isSorted = false;
        int lastUnSorted = array.length-1;
        while (!isSorted) {
            isSorted = true;
            for (int i=0; i<lastUnSorted; i++) {
                if(array[i]>array[i+1]) {
                   int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    isSorted = false;
                }
            }
            lastUnSorted--;
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int i=0;i<array.length;i++) {
            System.out.println(array[i]);
        }
    }


}
