public class EvenDigitSum {

    public static void main (String [] args) {

        getEvenDigitSum(-22);

    }



    public static int getEvenDigitSum (int num) {

        if (num <0){
            return -1;
        }

        int lastDigit;
        int sumEvenDigits = 0;

        while (num > 0){

            lastDigit = num%10;
            num = num - lastDigit;
            num /=10;
            if(lastDigit%2==0){
                sumEvenDigits += lastDigit;
                System.out.println(sumEvenDigits);
            }
        }
        return sumEvenDigits;
    }

}
