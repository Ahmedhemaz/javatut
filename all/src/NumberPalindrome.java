public class NumberPalindrome {


    public static void main (String [] args) {
        //12321
        //1
        //12
        //123
        //1232
        //12321

        System.out.println(palindromeNumber(121));

    }


    public static int reverse(int number) {

        int reversedNumber=0;
        int lastDigit;

        while (number > 0) {

            lastDigit = number % 10;
            number = number - lastDigit;
            number /=10;
            reversedNumber += lastDigit;
            reversedNumber *=10;
            //reversedNumber += lastDigit;
        }
        reversedNumber /=10;

        return reversedNumber;

    }

    public static boolean palindromeNumber (int num) {
        int reverseNumber = reverse(num);
        if(num == reverseNumber)
            return true;
        else
            return false;
    }

}
