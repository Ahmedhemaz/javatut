public class FirstAndLastDigitSum {


    public static void main (String [] args) {

        System.out.println(sumFirstAndLastDigit(2));

    }


     public static int sumFirstAndLastDigit (int num) {

        if(num < 0) {
            return -1;
        }
        if(num < 10) {
            return num * 2;
        }

            int firstNum = num%10;
            int lastDigit=0;
            while (num > 0 ) {
                lastDigit = num%10;
                num = num-lastDigit;
                num /=10;
            }


            return firstNum+lastDigit;

     }










}
