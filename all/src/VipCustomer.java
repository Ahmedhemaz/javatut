public class VipCustomer {

    private String customerName;
    private  int creditLimit;
    private String emailAddress;



    public VipCustomer() {

    this("Name",150,"email");

    }

    public VipCustomer(String name, int creditLimit) {

        this(name,creditLimit,"unknown@email.com");
    }

    public VipCustomer(String name, int creditLimit, String emailAddress) {
        this.customerName = name;
        this.creditLimit = creditLimit;
        this.emailAddress = emailAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
