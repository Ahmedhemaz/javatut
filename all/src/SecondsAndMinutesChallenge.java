public class SecondsAndMinutesChallenge {


    public static void main (String [] args) {

            getDurationString(500000);

    }



    private static int getDurationString (int min, int sec) {
        int hours;
        int secToMin ;
        int totalMin ;
        int remainingSecs;
        int remainingMins;
        if (min >=0 && sec>=0 && sec <=60) {
            secToMin = sec/60;
            remainingSecs = sec%60;
            totalMin = secToMin + min;
            hours = totalMin/60;
            remainingMins = totalMin%60;
            System.out.println(hours+"h"+remainingMins+"m"+remainingSecs+"s");
            return hours;

        } else {
            return -1;
        }
    }
    private static int getDurationString (int sec){
        int min = 0;
        int remainningSec = 0;
        if (sec>=0 ) {

            min = sec/60;
            remainningSec = sec%60;
            System.out.println(sec+"s"+"  has"+min+"m"+" and"+remainningSec+" s");

            return getDurationString(min,remainningSec);
        } else {
            return -1;
        }

    }

}
