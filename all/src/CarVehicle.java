public class CarVehicle extends Vehicle {

    private int gearPos;

    public CarVehicle(int gearPos) {
        this.gearPos = gearPos;
    }

    public void changingGear (int gearPos) {
        System.out.println("CarVehicle.changingGear() called");
    }

    public void steering () {

    }


}
