public class BankAccount {

    private String accountNumber;
    private int balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public BankAccount(String accountNumber, int balance, String customerName, String email
    , String phoneNumber) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;

    }

    public void setAccountNumber (String AccountNumber) {
        this.accountNumber = AccountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public  void  setBalance (int Balance) {
        this.balance = Balance;
    }

    public void newDeposit (int deposit) {
        this.balance += deposit;
    }

    public void newWithdraw (int withdraw) {
        if (withdraw > 0)
            this.balance -= withdraw;
    }

    public int getBalance() {
        return balance;
    }

    public void setCustomerName (String CustomerName) {
        this.customerName = CustomerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setEmail (String Email) {
        this.email = Email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhoneNumber (String PhoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}


