package com.company;


class Car {

    private boolean engine;
    private int cylinderNumber;
    private int wheelsNumber;
    private String name;


    public Car(int cylinderNumber, String name) {
        this.engine = true;
        this.cylinderNumber = cylinderNumber;
        this.wheelsNumber = 4;
        this.name = name;
    }

    public boolean isEngine() {
        return engine;
    }

    public int getCylinderNumber() {
        return cylinderNumber;
    }

    public int getWheelsNumber() {
        return wheelsNumber;
    }

    public String getName() {
        return name;
    }

    public String  startEngine () {

      return  "Car.startEngine -> called";

    }

    public String  accelerate () {
       return  "Car.accelerate -> called";
    }

    public String  brake () {
        return  "Car.brake -> called";
    }
}


class Astonmartin extends Car {

    public Astonmartin(int cylinderNumber, String name) {
        super(cylinderNumber, name);
    }

    public String startEngine () {

        return  "Astonmartin.startEngine -> called";

    }

    public String accelerate () {
        return "Astonmartin.accelerate -> called";
    }

    public String  brake () {
        return "Astonmartin.brake -> called";
    }


}


class Ford extends Car {
    public Ford(int cylinderNumber, String name) {
        super(cylinderNumber, name);
    }

    public String startEngine () {

        return "Ford.startEngine -> called";

    }

    public String accelerate () {
        return "Ford.accelerate -> called";
    }

    public String brake () {
        return "Ford.brake -> called";
    }

    public String borabora() {
        return "bora bora";
    }
}


class Wv extends Car {
    public Wv(int cylinderNumber, String name) {
        super(cylinderNumber, name);
    }

    public String startEngine () {

       return "Wv.startEngine -> called";

    }

    public String accelerate () {
        return "Wv.accelerate -> called";
    }

    public String brake () {
        return "Wv.brake -> called";
    }

}


public class Main {

    public static void main(String[] args) {
	// write your code here
//
//        for(int i=1 ; i< 10; i++) {
//            Car car = caro();
//            System.out.println("Car # "+ i
//                                + "type of engine " + car.startEngine() + "\n"
//                                + "accelerate type " + car.accelerate() + "\n"
//                                + "brake type " + car.brake()) ;
//
//        }
//

        Car ford = new Ford(10,"bora bora");
        System.out.println(((Ford)ford).borabora());

    }


    public static Car caro () {
        int randomNumber = (int) (Math.random() * 3) +1;
        switch (randomNumber) {
            case 1:
                return new Astonmartin(6,"astonmartin");
            case 2:
                return new Ford(4,"ford fusion");
            case 3:
                return new Wv(8,"WV basat");

        }
        return null;

    }



}























