package com.company;

public class Test {
    private int x;
    public Test(){
        this.x = 300;
    }
    public void myMethod(final String str, final int a){
        final int b;
        class MyLocalInnerClass{
            public void aMethod(){
                System.out.println(str+" "+a+" "+x+"aMethod here");

            }
        }
        MyLocalInnerClass myObj = new MyLocalInnerClass();
        myObj.aMethod();
    }
}
