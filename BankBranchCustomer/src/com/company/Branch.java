package com.company;

import java.util.ArrayList;

public class Branch extends Bank {

    private String branchName;
    private ArrayList<Customer> customersList;

    public Branch(String bankName, String phoneNumber, String postalCode, String address, String branchName) {
        super(bankName, phoneNumber, postalCode, address);
        this.branchName = branchName;
        this.customersList= new ArrayList<Customer>();
    }

    public int addcustomer(String firstName, String lastName){
        if(customersList.add(new Customer(firstName,lastName))){
            return 1;
        }else{
            return 0;
        }
    }

    public String getBranchName() {
        return branchName;
    }

    public ArrayList<Customer> getCustomersList() {
        return customersList;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
