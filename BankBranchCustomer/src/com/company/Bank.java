package com.company;

import java.util.ArrayList;

public class Bank {

    private String bankName;
    private String phoneNumber;
    private String postalCode;
    private String address;
    private ArrayList<Branch> branchesList;

    public Bank(String bankName, String phoneNumber, String postalCode, String address) {
        this.bankName = bankName;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.address = address;
        this.branchesList = new ArrayList<Branch>();
    }

    public boolean addBranch( String phoneNumber, String postalCode, String address, String branchName){

        boolean exist = containsBranch(branchName);

        if(!exist){
            if(branchesList.add(new Branch(this.bankName,phoneNumber,postalCode,address,branchName))){
                return true;
            }
        }
        return false;

    }

    public boolean addcustomer(String branchName,String firstName, String lastName){
        Branch branch = getbranch(branchName);
        if(branch.getCustomersList().add(new Customer(firstName,lastName))){
            return true;
        }else{
            return false;
        }
    }

    private boolean containsBranch (String branchName){
        for (Branch temp:branchesList
                ) {
            if(temp.getBranchName()== branchName){
                return true;
            }
        }
        return false;

    }

    public Branch getbranch(String branchName) {
        for (Branch temp:branchesList
             ) {
            if(temp.getBranchName()==branchName){
                return temp;
            }
        }
        return null;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
