package com.company;

import java.util.ArrayList;

public class Customer {
    private String firstName;
    private String lastName;
    private ArrayList<Double> transactions ;

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.transactions = new ArrayList<Double>();
    }

    public int addtransaction (double transaction){
        if(this.transactions.add(transaction)){
            return 1;
        } else {
            return 0;
        }
    }

    public void editTransactions(double oldTransaction,double newTransaction) {
        int transactionIndex = this.transactions.indexOf(oldTransaction);
        this.transactions.set(transactionIndex,newTransaction);
    }

    public void printtransactions(){
        int transactionsSize = this.transactions.size();
        for(int i=0;i<transactionsSize;i++) {
            System.out.println(this.transactions.get(i));
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }


}