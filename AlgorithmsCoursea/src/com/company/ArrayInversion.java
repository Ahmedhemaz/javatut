package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class ArrayInversion {

    public static void main(String[] args) {
        Scanner scanner = null;
        int[]  array = new int[100000];
        try {
            scanner = new Scanner(new File("./src/com/company/_bcb5c6658381416d19b01bfc1d3993b5_IntegerArray.txt"));
            int i = 0;
            while(scanner.hasNextInt()){
                array[i++] = scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int[] intArray = {1, 20, 6, 4, 5,65,-134,865,-45641,5641,-54215421,54210,-7485641,54231,0,-8,544120};
        System.out.println(countInversion(array));
//        printArray(array);
    }


    public static long countInversion(int [] array) {
       return split(array,0,array.length);
    }


    public static long split (int[] array, int start, int end) {
        if(end - start < 2) {
            return 0;
        } else {
            long inversionCounter = 0;
            int mid = (end + start)/2 ;
            inversionCounter = split(array,start,mid);
            inversionCounter += split(array,mid,end);
            inversionCounter += mergeHalves(array,start,mid,end);
            return inversionCounter;
        }
    }

    public static long mergeHalves (int[] array, int start, int mid ,int end) {
        if(array[mid-1]<=array[mid]){
            return 0;
        } else {
            int i = start;
            int j = mid ;
            int tempIndex = 0;
            int[] tempArray = new int[end - start];
            long inversionCounter = 0;

            while (i<mid && j<end) {
//                tempArray[tempIndex++] = array[i]<=array[j]?array[i++]:array[j++];

                if(array[i] <= array[j]){
                    tempArray[tempIndex++] = array[i++];
                }else {
                    inversionCounter += mid-i;
                    tempArray[tempIndex++] = array[j++];
                }
            }
            // to handle left over elements we didn't copy them to temp array
            System.arraycopy(array,i,array,start+tempIndex,mid-i);
            // copy copied sorted temp array  elements  to the array
            System.arraycopy(tempArray,0,array,start,tempIndex);

            return  inversionCounter;
        }

    }

    public static void printArray(int[] array) {
        for (int i=0; i<array.length; i++) {
            System.out.printf(array[i]+", ");
        }
        System.out.println("");
    }
}
