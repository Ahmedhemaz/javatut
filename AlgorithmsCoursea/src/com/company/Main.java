package com.company;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //3141592653589793238462643383279502884197169399375105820974944592
//        2718281828459045235360287471352662497757247093699959574966967627
//        System.out.println(calculateNumberBase(123));
        BigInteger firstNumber = new BigInteger("3141592653589793238462643383279502884197169399375105820974944592");
        BigInteger secondNumber = new BigInteger("2718281828459045235360287471352662497757247093699959574966967627");
//        System.out.println(calculateNumberBase(secondNumber));
//        System.out.println(multiply(123,456)) ;
        System.out.println(multiply(firstNumber,secondNumber));
    }

    public static int multiply (int firstNumber, int secondNumber){
        if (firstNumber/10 < 1 && secondNumber/10 < 1){
            return 1;
        }else {
            multiply(firstNumber% calculateNumberBase(firstNumber),secondNumber%calculateNumberBase(secondNumber));
            return calculateMergeResult(firstNumber,secondNumber);
        }
    }

    public static int calculateMergeResult(int firstNumber, int secondNumber){

        int z2,z1,z0,n;
        n = calculateNumberBase(firstNumber);
        z2 = (firstNumber/n)*(secondNumber/n);
        z0 = (firstNumber%n) * (secondNumber%n);
        z1 = ((firstNumber/n)*(secondNumber%n)) + ((secondNumber/n)*(firstNumber%n));
        return (int) (z2*(Math.pow(n,2)) + z1*n + z0);
    }

    public static int calculateNumberBase(int number){

        int n=0;
        while (number/Math.pow(10,n) > 1){
            n++;
        }
        return (int) Math.pow(10,n-1);

    }

    public static BigInteger calculateNumberBase(BigInteger number){
        int n = 0;
        BigInteger x = new BigInteger("10");
        while (number.divide(x.pow(n)).compareTo(x) == 1){
            n++;
        }
        return x.pow(n);

    }

    public static BigInteger calculateMergeResult(BigInteger firstNumber, BigInteger secondNumber){
        BigInteger z2,z1,z0,n;
        n = calculateNumberBase(firstNumber);
        z2 = (firstNumber.divide(n)).multiply(secondNumber.divide(n));
        z0 = (firstNumber.mod(n)).multiply(secondNumber.mod(n));
        z1 = (firstNumber.divide(n)).multiply(secondNumber.mod(n)).add((secondNumber.divide(n)).multiply(firstNumber.mod(n)));
        return  (z2.multiply(n.pow(2))).add((z1.multiply(n))).add(z0);

    }

    public static BigInteger multiply (BigInteger firstNumber, BigInteger secondNumber){
        BigInteger x = new BigInteger("10");
        BigInteger k = calculateNumberBase(firstNumber);
        if (firstNumber.divide(x).compareTo(x) == -1 && secondNumber.divide(x).compareTo(x) == -1){
            return BigInteger.valueOf(1);
        }else {
            multiply(firstNumber.mod(k),secondNumber.mod(k));
            return calculateMergeResult(firstNumber,secondNumber);
        }
    }

}
