package com.company;

public class Furniture {
    private int chair;
    private int bed;
    private int TV;
    private int table;

    public Furniture(int chair, int bed, int TV, int table) {
        this.chair = chair;
        this.bed = bed;
        this.TV = TV;
        this.table = table;
    }

    public int getChair() {
        return chair;
    }

    public int getBed() {
        return bed;
    }

    public int getTV() {
        return TV;
    }

    public int getTable() {
        return table;
    }
}
