package com.company;

public class Room {
    private int lenght;
    private int weidth;
    private int height;
    private int numberOfWindows;
    private Furniture furniture;
    private String roomType;

    public Room(int lenght, int weidth, int height, int numberOfWindows, Furniture furniture, String roomType) {
        this.lenght = lenght;
        this.weidth = weidth;
        this.height = height;
        this.numberOfWindows = numberOfWindows;
        this.furniture = furniture;
        this.roomType = roomType;
    }

    public int getLenght() {
        return lenght;
    }

    public int getWeidth() {
        return weidth;
    }

    public int getHeight() {
        return height;
    }



    public int getNumberOfWindows() {
        return numberOfWindows;
    }

    public Furniture getFurniture() {
        return furniture;
    }

    public String getRoomType() {
        return roomType;
    }
}
