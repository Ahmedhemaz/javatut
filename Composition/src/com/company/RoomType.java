package com.company;

public class RoomType {

    private Room bedRoom;
    private Room bathRoom;
    private Room livingRoom;

    public RoomType(Room bedRoom, Room bathRoom, Room livingRoom) {
        this.bedRoom = bedRoom;
        this.bathRoom = bathRoom;
        this.livingRoom = livingRoom;
    }

    public Room getBedRoom() {
        return bedRoom;
    }

    public Room getBathRoom() {
        return bathRoom;
    }

    public Room getLivingRoom() {
        return livingRoom;
    }
}
