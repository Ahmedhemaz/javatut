package com.company;

import java.math.*;
import java.lang.Long;


public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println(super_digit("861568688536788989894564641",100000));
    }

    public static long super_digit(String StringSuperDigit,int k) {
        if(k>=1){
            long superDigit = Long.parseLong(StringSuperDigit);
            Long firstSuperDigit = super_digit(superDigit)*k;
            return super_digit(firstSuperDigit);
        } else
            return 0;
    }

    public static Long super_digit(Long superDigit) {
        if(superDigit<10) {
            return superDigit;
        } else {
            Long sum;
            for(sum= Long.valueOf(0); superDigit>0; sum+=superDigit%10,superDigit/=10);
            return super_digit(sum);
        }
    }


}
