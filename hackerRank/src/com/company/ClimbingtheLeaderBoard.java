package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class ClimbingtheLeaderBoard {

    public static void main(String[] args){
        ArrayList<Integer> leaderBoard = new ArrayList<>();
        Integer[] scoresTwo = {100,90,90,80,75,60};
        Integer[] scoresOne = {100,100,50,40,40,20,10};
        Integer[] aliceOne = {5,25,50,120};
        Integer[] aliceTwo = {50,65,77,90,102};
        addToBoard(scoresOne,leaderBoard);
        leaderBoardRanking(leaderBoard,aliceOne);

    }

    public static void leaderBoardRanking(ArrayList<Integer> leaderBoardArray, Integer[] alice){
        List<Integer> nonDuplicateList = leaderBoardArray.stream().distinct().collect(Collectors.toList());
        int size = nonDuplicateList.size();
       for (int i= 0; i<alice.length; i++ ){
           if(size < nonDuplicateList.size()){
               nonDuplicateList.remove(alice[i-1]);
               if(!nonDuplicateList.contains(alice[i])){
                   nonDuplicateList.add(alice[i]);
               }
           }else {

               nonDuplicateList.add(alice[i]);
           }
//           printArray(leaderBoardArray);
//           System.out.println("\n");
           nonDuplicateList.sort(Collections.reverseOrder());
           System.out.println(nonDuplicateList.indexOf(alice[i])+1);
           printArray(nonDuplicateList);
           System.out.println("\n");
       }
//       printArray(leaderBoardArray);
    }


    public static void addToBoard(Integer[] scores, ArrayList<Integer> leaderBoardArrayList){
        Collections.addAll(leaderBoardArrayList, scores);
    }

    public static void printArray(List<Integer> leaderBoard){
        for (Integer integer : leaderBoard) {
            System.out.printf(integer+",");
        }
    }




}



