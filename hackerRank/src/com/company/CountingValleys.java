package com.company;

public class CountingValleys {
    public static void main(String[] args) {

        char [] charArray=
        {'D','D','U','U','D','D','U','D','U','U','U','D'};
        String s = "UDDDUDUU";
        int valleysCounter = 0;
        int currentLevel = 0;
        int lastlevel = 0;
        for (int i = 0 ; i < s.length() ; i ++){
            switch (s.charAt(i)){
                case 'D': currentLevel--;
                break;
                case 'U': currentLevel++;
            }
            if (lastlevel < 0 && currentLevel == 0){
                valleysCounter ++;
            }
            lastlevel = currentLevel;
        }
        System.out.println(valleysCounter);
    }
}
