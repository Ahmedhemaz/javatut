package com.company;

import java.util.HashMap;

public class SockMerchant {
    public static void main(String[] args) {
        int[] arr = {10, 20, 20, 10, 10, 30, 50, 10, 20};
        int pairCounter = 0;
        HashMap<Integer,Integer> integerHashMap = new HashMap<>();
        for (int i = 0 ; i < arr.length ; i ++){
            if (integerHashMap.containsKey(arr[i])){
                if (integerHashMap.get(arr[i]) == 1){
                    pairCounter++;
                    integerHashMap.put(arr[i],0);
                }else {
                    integerHashMap.put(arr[i],integerHashMap.get(arr[i])+1);
                }
            }else{
                integerHashMap.put(arr[i],1);
            }
        }

        System.out.println(pairCounter);
    }
}
