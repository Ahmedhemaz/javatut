package com.company;

import java.lang.String;



public class Main {

    public static void main(String[] args) {

        String s = "chillout";
//        String[] charArray = new String[s.length()];
//        charArray = s.split("");
//        printArray(charArray);
          printMultiArray(encryption(s),(int)Math.sqrt(s.length())+1,(int)Math.sqrt(s.length()+1));

    }


    public static char[][]  encryption(String s) {
        int row = (int)Math.sqrt(s.length());
        int column = (int)Math.sqrt(s.length())+1;
        if(row*column<s.length()){
            row = column;
        }
        int k=0;
        char[][] encryptionArray = new char[row][column];
        for(int i=0;i<row;i++){
            int j=0;
            for(j=0;j<column;j++){
                if((k+j) < s.length()){
                    encryptionArray[i][j]=s.charAt(k+j);
                }
            }
            k+=j;
        }
        return encryptionArray;
    }

    public static void printMultiArray(char[][] array,int column,int row){
        for(int i=0;i<column;i++){
            for (int j=0;j<row;j++){
                if(array[j][i] == '\u0000') {
                    continue;
                }
                System.out.printf(""+array[j][i]);
            }
        }
    }


    public static void printArray(String[] charArray){
        for(int i=0;i<charArray.length;i++){
            System.out.println(charArray[i]);
        }
    }
}
