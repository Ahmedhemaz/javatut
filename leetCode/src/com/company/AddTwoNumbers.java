package com.company;

import java.util.Iterator;
import java.util.LinkedList;

public class AddTwoNumbers {

    public static void main(String[] args){

        LinkedList<Integer> l1 = new LinkedList<>();
        l1.add(2);
        l1.add(4);
        l1.add(3);
        LinkedList<Integer> l2 = new LinkedList<>();
        l2.add(5);
        l2.add(6);
        l2.add(4);
        LinkedList<Integer> reversedLinkedList = new LinkedList<>();
        printLinkedList(addReverseNumberToLinkedList(addTwoNumbersOfLinkedLists(addLinkedListNumbers(l1),
                addLinkedListNumbers(l2)),reversedLinkedList));
    }


    public static Integer addLinkedListNumbers(LinkedList<Integer> l1){
        Integer l1Sum =0;
        Iterator<Integer> l1Iterator = l1.iterator();
        while (l1Iterator.hasNext()){
            l1Sum += l1Iterator.next();
            l1Sum *=10;
        }
        return reverseNumber(l1Sum/10);
    }

    public static Integer addTwoNumbersOfLinkedLists(Integer l1RevSum, Integer l2RevSum){
        return l1RevSum+l2RevSum;
    }

    public  static Integer reverseNumber(Integer number){
        Integer reversedNumber = 0, mod;
        while (number > 0){
            mod = number%10;
            reversedNumber +=mod;
            reversedNumber *=10;
            number -=mod;
            number /=10;
        }
        return reversedNumber/10;
    }

    public static LinkedList<Integer> addReverseNumberToLinkedList(Integer reversedNumber, LinkedList<Integer> reversedLinkedList){
        Integer mod ;
        while (reversedNumber > 0){
            mod = reversedNumber%10;
            reversedLinkedList.add(mod);
            reversedNumber -=mod;
            reversedNumber /=10;
        }
        return reversedLinkedList;
    }

    public static void printLinkedList(LinkedList<Integer> linkedList){
        for (Integer integer : linkedList) {
            System.out.print(integer + "->");
        }
    }

}
