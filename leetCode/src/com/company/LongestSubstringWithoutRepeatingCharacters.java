package com.company;

import java.util.HashSet;
import java.util.Iterator;

public class LongestSubstringWithoutRepeatingCharacters {

    public static void main(String[] args){

        System.out.println(subStringLength("anviaj"));

    }


    public static int subStringLength(String s){
        HashSet<Character> substringHashSet = new HashSet<>();
        int substringHashSetLength = 0;
        int LongestSubString = 0;
        char lastCh = 0;
        if(s.equals(" ")){
            return 1;
        }
        for (char ch  : s.toCharArray()) {
            if(substringHashSet.add(ch)){
                substringHashSetLength++;
            }else{
                if(lastCh == ch){
                    substringHashSet.clear();
                    substringHashSet.add(ch);
                    substringHashSetLength = 1;
                }else {
                    substringHashSet.clear();
                    substringHashSet.add(ch);
                    substringHashSet.add(lastCh);
                    substringHashSetLength = 2;
                }

            }
            if(substringHashSetLength > LongestSubString){
                LongestSubString = substringHashSetLength;
            }
            lastCh = ch;
        }

        return LongestSubString;
    }


    public static void printHashSet (HashSet hashSet){
        Iterator itr = hashSet.iterator();
        while (itr.hasNext()){
            System.out.print(itr.next() + "->");
        }
    }



}
