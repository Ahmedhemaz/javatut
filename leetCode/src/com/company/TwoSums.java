package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class TwoSums {

    public static void main(String[] args) {
        int [] numbers = {7,2,11,15,6,3,8,1,9,0,5};

        HashMap<Integer,Integer> integerHashMap = new HashMap<>();
        fillIntegerHashMapFromArray(numbers,integerHashMap);
        Vector<Integer> arrayList = findTwoSums(9,numbers,integerHashMap);
        for (int number :
                arrayList) {
            System.out.println(numbers[number]);
        }

    }


    public static void fillIntegerHashMapFromArray(int[] array, HashMap<Integer,Integer> integerHashMap){
        for (int i = 0 ; i < array.length; i++){
            integerHashMap.put(array[i],i);
        }
    }

    public static Vector<Integer> findTwoSums(int target, int[] array, HashMap<Integer,Integer> integerHashMap){
        Vector<Integer> integerArrayList = new Vector<>();
        for (int i = 0 ; i < array.length; i++){
            if (integerHashMap.get(target-array[i]) != null){
                integerArrayList.add(integerHashMap.get(target-array[i]));
            }
        }
        return integerArrayList;
    }

}
