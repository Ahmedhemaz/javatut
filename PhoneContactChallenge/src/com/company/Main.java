package com.company;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone();

    public static void main(String[] args) {

        boolean quit = false;
        int choice = 0;
        printInstructions();
        while (!quit){
            System.out.println("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();


            switch (choice) {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    showAllContacts();
                    break;
                case 2:
                    addContact();
                    break;
                case 3:
                    modifyContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    findContact();
                    break;
                case 6:
                    quit = true;
                    break;
            }


        }


    }


    public static void printInstructions() {
        System.out.print("\nPress ");
        System.out.print("\n 0 - To print choice options.");
        System.out.print("\n 1 - To print contacts ");
        System.out.print("\n 2 - To add new contact");
        System.out.print("\n 3 - To modify contact.");
        System.out.print("\n 4 - To remove contact.");
        System.out.print("\n 5 - To find contact.");
        System.out.println("\n 6 - To quit the application.");
    }



    public static void addContact () {
        System.out.println("Please enter the name : ");
        String name = scanner.nextLine();
        System.out.println("Please enter the number : ");
        String number = scanner.nextLine();
        Contact contact = new Contact(name,number);
        mobilePhone.addContact(contact);

    }


    public static void showAllContacts () {
        mobilePhone.printArray();
    }

    public static void findContact () {
        System.out.println("Please enter name to be found");
        String contactName = scanner.nextLine();
        mobilePhone.searchContact(contactName);
    }

    public static void modifyContact () {
        System.out.println("Please enter name to modify: ");
        String currentName = scanner.nextLine();
        System.out.println("Please enter the new name: ");
        String newName = scanner.nextLine();
        if(newName.isEmpty())
            newName = "";
        System.out.println("Please enter the new number");
        String newNumber = scanner.nextLine();
        if(newNumber.isEmpty())
            newNumber = "";
        mobilePhone.modifyContact(currentName,newName,newNumber);
    }

    public static void removeContact () {
        System.out.println("Please enter name to remove contact: ");
        String contactName = scanner.nextLine();
        mobilePhone.removeContact(contactName);
    }

}
