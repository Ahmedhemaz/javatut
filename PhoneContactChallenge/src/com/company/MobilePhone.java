package com.company;

import java.util.ArrayList;

public class MobilePhone {

  private   ArrayList<Contact> ContactList = new ArrayList<Contact>();

  public void printArray () {
      System.out.println("you have "+ ContactList.size() + " items in your grocery list");
      for (int i = 0; i<ContactList.size(); i++){
          System.out.println((i+1) + ". " + " Name: " + ContactList.get(i).getName()+"  Number:"+ContactList.get(i).getNumber());
      }

  }

  public void addContact (Contact contact) {

      ContactList.add(contact);
  }




  public void modifyContact (String currentContact , String newContact, String newNumber) {
      int position = findIndex(currentContact);
      if (position >=0 ) {
          if(newContact == "")
              newContact = ContactList.get(position).getName();
          modifyContactName(position,newContact);
          if (newNumber == "")
              newNumber = ContactList.get(position).getNumber();
          modifyContactNumber(position,newNumber);
      }
  }

  private void modifyContactName (int position , String newContact) {
      ContactList.get(position).setName(newContact);
  }
  private void modifyContactNumber (int position,String newNumber) {
      ContactList.get(position).setNumber(newNumber);

  }

  public void removeContact (String contactName) {
      ContactList.remove(findIndex(contactName));
  }

  public void searchContact (String contactName) {
      System.out.println("Name: " + ContactList.get(findIndex(contactName)).getName() +"  Number: " + ContactList.get(findIndex(contactName)).getNumber());
  }

  private int findIndex (String contactName) {
      return ContactList.indexOf(findContact(contactName));
  }

    private Contact findContact(String name) {
        for(Contact contact : ContactList) {
            if(contact.getName().equals(name)) {
                return contact;
            }
        }
        return null;
    }
}
