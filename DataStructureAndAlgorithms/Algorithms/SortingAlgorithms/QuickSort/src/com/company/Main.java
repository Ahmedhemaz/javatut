package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] intArray = {20,35,-15,7,55,1,-22};
        printArray(intArray);
        quickSort(intArray);
        printArray(intArray);
    }
    public static void quickSort(int[] array) {
        qsort(array,0,array.length-1);
    }

    public static void qsort(int[] array, int left, int right){
        if(left>=right){
            return;
        } else {
            int pivot = array[(left+right)/2];
            int index = partation(array,left,right,pivot);
            qsort(array,left,index-1);
            qsort(array,index,right);
        }
    }

    public static int partation(int[] array, int left, int right,int pivot){
        while (left <= right){
            while (array[left] < pivot) {
                left ++;
            }
            while (array[right] > pivot) {
                right--;
            }
            if(left <= right) {
                swap(array,left,right);
                left++;
                right--;
            }

        }
        return left;

    }

    public static void swap(int[] array, int left, int right) {
        int temp = array[left];
        array[left]= array[right];
        array[right]=temp;
    }

    public static void printArray (int[] array) {
        for(int i=0; i<array.length; i++){
            System.out.printf(array[i]+", ");
        }
        System.out.println(" ");
    }

}
