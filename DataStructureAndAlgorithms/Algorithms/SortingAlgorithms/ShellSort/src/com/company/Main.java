package com.company;

public class Main {

    public static void main(String[] args) {

        int[] intArray = {20,35,-15,7,55,1,-22};
        printArray(intArray);
        shellSort(intArray);
        printArray(intArray);
    }

    public static void shellSort (int[] array) {

        for(int gap=array.length/2; gap>0; gap /=2) {

            for(int i=gap; i< array.length; i++){
                int newElement = array[i];

                int j = i;

                while (j>=gap && array[j-gap]>newElement) {
                    array[i] = array[j-gap];
                    j -=gap;
                }
                array[j]=newElement;
            }
        }
    }

    public static void printArray (int[] array) {
        for(int i=0; i<array.length; i++) {
            System.out.printf(array[i] + ", ");
        }
        System.out.println("");
    }
}
