package com.company;

public class Main {

    public static void main(String[] args) {

        int[] intArray = {20,35,-15,7,55,1,-22};

        printArray(intArray);
        System.out.println("");
        selectionSort(intArray);
        printArray(intArray);

    }

    public static void selectionSort(int[] array) {

        for(int lastUnsortedIndex=array.length-1;lastUnsortedIndex>0;lastUnsortedIndex--) {
            int largest =0;
            for(int i=1;i<=lastUnsortedIndex;i++){
                if(array[i]>array[largest]){
                    largest=i;
                }
            }
            swap(array,largest,lastUnsortedIndex);
        }
    }

    public static void printArray (int[] array) {
        for(int i=0; i<array.length; i++) {
            System.out.printf(array[i] + ", ");
        }
    }

    public static void swap (int[] array, int i, int j) {
        if(i==j){
            return;
        }else{
            int temp = array[i];
            array[i]=array[j];
            array[j]=temp;
        }
    }



}
