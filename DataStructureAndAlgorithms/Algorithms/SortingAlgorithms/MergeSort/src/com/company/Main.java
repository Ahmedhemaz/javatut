package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] intArray = {1, 20, 6, 4, 5};
        printArray(intArray);
        mergeSort(intArray);
        printArray(intArray);
    }

    public static void mergeSort(int [] array) {
        split(array,0,array.length);
    }


    public static int split (int[] array, int start, int end) {
        if(end - start < 2) {
            return 0;
        } else {
            int inversionCounter = 0;
            int mid = (end + start)/2 ;
            inversionCounter = split(array,start,mid);
            inversionCounter += split(array,mid,end);
            inversionCounter += mergeHalves(array,start,mid,end);
            System.out.println(inversionCounter);
            return inversionCounter;
        }
    }

    public static int mergeHalves (int[] array, int start, int mid ,int end) {
        if(array[mid-1]<=array[mid]){
            return 0;
        } else {
            int i = start;
            int j = mid ;
            int tempIndex = 0;
            int[] tempArray = new int[end - start];
            int inversionCounter = 0;

            while (i<mid && j<end) {
//                tempArray[tempIndex++] = array[i]<=array[j]?array[i++]:array[j++];

                if(array[i] <= array[j]){
                    tempArray[tempIndex++] = array[i++];
                }else {
                    inversionCounter += mid-i;
                    tempArray[tempIndex++] = array[j++];
                }
            }
            System.out.println(inversionCounter);
            // to handle left over elements we didn't copy them to temp array
            System.arraycopy(array,i,array,start+tempIndex,mid-i);
            // copy copied sorted temp array  elements  to the array
            System.arraycopy(tempArray,0,array,start,tempIndex);

            return  inversionCounter;
        }

    }

    public static void printArray(int[] array) {
        for (int i=0; i<array.length; i++) {
            System.out.printf(array[i]+", ");
        }
        System.out.println("");
    }

    public static void merge(int[] array, int start, int mid ,int end){
        int i = start;
        int j = mid+1 ;
        int tempIndex = 0;
        int[] tempArray = new int[array.length];

        while (i<=mid && j<=end) {
            if(array[i]<=array[j]){
                tempArray[tempIndex++]=array[i++];
            }else{
                tempArray[tempIndex++]=array[j++];
            }
        }
        printArray(tempArray);
        while (i<=mid){
            tempArray[tempIndex++]=array[i++];
        }
        while (j<=end){
            tempArray[tempIndex++]=array[j++];
        }
        for (tempIndex=start;tempIndex<=end;tempIndex++){
            array[tempIndex]=tempArray[tempIndex];
        }

    }


}
