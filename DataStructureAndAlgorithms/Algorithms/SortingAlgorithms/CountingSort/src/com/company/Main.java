package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] intArray = {2, 5, 9, 8, 2, 8, 7, 10, 4, 3,11,6};
        printArray(intArray);
        countingSort(intArray);
        printArray(intArray);

    }

    public static void countingSort(int[] array){
        countSort(array,0,array.length);
    }

    public static void countSort(int[] array, int start, int end) {
        int[] countArray = new int[(end-start)+1];
        for (int i=0; i<array.length;i++){
            countArray[array[i]]++;
        }
        int k = 0;

        for(int j=0;j<=array.length;j++){
            if(countArray[j]==0){
                continue;
            } else {
                while (countArray[j]>0){
                    array[k++]=j;
                    countArray[j]--;
                }
            }
        }
    }


    public static void printArray(int[] array) {
        for(int i=0; i<array.length; i++) {
            System.out.printf(array[i]+", ");
        }
        System.out.println(" ");
    }

}
