package com.company;

public class Main {

    public static void main(String[] args) {
        int[] intArray = {20,35,-15,7,55,1,-22};
        printArray(intArray);
        insertionSort(intArray);
        printArray(intArray);
    }

    public static void insertionSort (int[] array) {

        for(int firstUnsortedIndex=1; firstUnsortedIndex<array.length;firstUnsortedIndex++){
            int newElement = array[firstUnsortedIndex];
            int i = firstUnsortedIndex;
          while (i>0&&array[i-1]>newElement){
              array[i] = array[i-1];
              i--;
          }
            array[i] = newElement;
        }
    }

    public static void printArray (int[] array) {
        System.out.println("");
        for(int i=0; i<array.length; i++) {
            System.out.printf(array[i] + ", ");
        }
    }
}
