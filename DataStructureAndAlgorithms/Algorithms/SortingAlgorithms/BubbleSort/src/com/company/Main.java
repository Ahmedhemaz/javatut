package com.company;

public class Main {

    public static void main(String[] args) {
        int[] intArray = {35,-15,1,55,-20,30,15};

        for(int i = 0;i<intArray.length;i++){
            System.out.printf(intArray[i] + ", ");
        }

       // bubbleSort(intArray);
        whileBubleSort(intArray);
        System.out.println(" ");

        for(int i = 0;i<intArray.length;i++){
            System.out.printf(intArray[i] + ", ");
        }
    }


    public static void bubbleSort (int[] array) {

        for(int lastUnSortedNumber = array.length-1;lastUnSortedNumber>0;lastUnSortedNumber-- ){
            for (int i=0;i<lastUnSortedNumber;i++){
                if(array[i]>array[i+1]) {
                    swap(array,i,i+1);
                }
            }
        }

    }

    public static void whileBubleSort (int[] array) {
        boolean isSorted = false;
        int boundry = array.length-1;
        while(!isSorted){
            isSorted = true;
            for(int i=0; i<boundry; i++) {
                if(array[i]>array[i+1]){
                    swap(array,i,i+1);
                    isSorted = false;
                }
            }
            boundry--;
        }
    }


    public static void swap (int[] array,int i , int j) {

            int temp = array[i] ;
            array[i] = array[j];
            array[j] = temp;

    }



}
