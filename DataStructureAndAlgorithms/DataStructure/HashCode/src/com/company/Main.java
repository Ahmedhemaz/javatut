package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        String key = "this";
//        int len;
//        len = key.length();
//        System.out.println(len);
//        System.out.println(hashCode("this"));
//        System.out.println(hashCode("shit"));
//        System.out.println(hashCode("itsh"));
//        System.out.println(hashCode("hits"));

//        int index;

        //      index = convertToPostive(hashCode("this"),6);
        //    System.out.println(index);

        String[] array = new String[6];
        addWithLinearProbing(array,"this");
        addWithLinearProbing(array,"shit");
        addWithLinearProbing(array,"hits");
        printArray(array);
        System.out.println(search(array,"hits"));


}



public static int hashCode (String s) {
    int hashValue = 0;
    int c = 31;
    for (int index =0; index < s.length() ; index++) {
        hashValue += s.charAt(index)*Math.pow(c,index);
    }
    return hashValue;

    }
    public static int convertToPostive (int hashValue,int tableSize) {
        hashValue = hashValue & 0x7fffffff;
        hashValue = hashValue%tableSize;

        return hashValue;
    }

    public static String[] addWithLinearProbing (String [] array , String input ) {
        int index = convertToPostive(hashCode(input),array.length);
        while (array[index] != null) {
            index++;
        }
        array[index]=input;
        return array;
    }


    public static int search (String[] array ,String input) {
        int index = convertToPostive(hashCode(input), array.length);
        while (array[index] != input) {
            index++;
        }
        return index;
    }

    public static void printArray(String[] array) {
        for (int i=0;i<array.length;i++) {
            System.out.println(array[i]);
        }
    }

}
