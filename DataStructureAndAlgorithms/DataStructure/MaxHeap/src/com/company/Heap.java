package com.company;

public class Heap<E> {
    private E[] array;
    private int size;
    private int lastPosition;
    private int sortPosition;

    public Heap(int size) {
        // uncheck object cating
        this.array = (E[]) new Object[size];
        this.size = size;
        this.lastPosition = 0;
    }


    public void add(E obj){
        array[lastPosition]=obj;
        trickleUp(lastPosition);
        lastPosition++;
        sortPosition = lastPosition-1;
    }

    private void swap (int from, int to ){
        E temp = array[from];
        array[from]=array[to];
        array[to]=temp;
    }


    private void trickleUp(int position){
        if (position==0){
            return;
        }
        int parent = (int) Math.floor((position-1)/2);
        // uncheck  object casting ------------> check
        if (((Comparable<E>)array[position]).compareTo(array[parent])>0){
            swap(position,parent);
            trickleUp(parent);
        }

    }

    public void printHeap(){
        for (int i=0;i<lastPosition;i++){
            System.out.println(" \n index"+i+"="+array[i]);
        }
    }

    public E remove(){
        E temp = array[0];
        System.out.println(lastPosition);
        lastPosition--;
        System.out.println(lastPosition);
        sortPosition = lastPosition-1;
        swap(0,lastPosition);
        trickleDown(0,lastPosition);
        return temp;
    }

    private void trickleDown(int parent,int position){
        int left = (2*parent)+1;
        int right = (2*parent)+2;

        if(left == position-1 && (((Comparable<E>)array[parent]).compareTo(array[left]))<0) {
            swap(parent,left);
            System.out.printf("lastPosition parent left \n");
            return;
        }
        if(right == position-1 && (((Comparable<E>)array[parent]).compareTo(array[right]))<0){
            swap(parent,right);
            System.out.println("lastPosition parent right \n");
            return;
        }
        if(left>=position || right>=position){
            return;
        }
        if(((((Comparable<E>)array[left]).compareTo(array[right]))>0) && (((Comparable<E>)array[parent]).compareTo(array[left]))<0){
            swap(parent,left);
            System.out.printf("left right parent left \n");
            trickleDown(left,position);

        }else if((((Comparable<E>)array[parent]).compareTo(array[right]))<0){
            swap(parent,right);
            System.out.printf("left right parent right \n");
            trickleDown(right,position);
        }
    }


    public void heapsort(){

        heapsort(sortPosition,array);
    }


    private void heapsort(int sortPosition, E[] array){
        if(sortPosition==0){
            return;
        }
        swap(0,sortPosition);
        sortPosition--;
        trickleDown(0,sortPosition);
        heapsort(sortPosition,array);
    }

}
