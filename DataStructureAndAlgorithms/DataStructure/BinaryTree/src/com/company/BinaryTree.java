package com.company;

public class BinaryTree<E> {
    class Node<E> {
         E data;
         Node<E> left , right ;

        public Node(E data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }
    }

    private Node<E> root = null;
    private int currentSize = 0;


    private void add(E obj, Node<E> node) {
        if(((Comparable<E>)obj).compareTo(node.data)>0) {
            //if obj > node.data go to the right
            if (node.right==null){
                node.right = new Node<E>(obj);
                return;
            }
             add(obj , node.right);
        }

        if(((Comparable<E>)obj).compareTo(node.data)<0){
            if(node.left == null){
                node.left = new Node<E>(obj);
                return;
            }
            // if less than node.data go to left
            add(obj,node.left);
        }

    }


    public void add (E obj){
        if(root == null){
            // if empty tree create root
            root = new Node<E>(obj);
        }else {
            // if not empty recursive add call
            add(obj,root);
            currentSize ++;
        }
    }


    private boolean contain (E obj,Node<E> node){
        // tree is empty
        if(node == null){
            return false;
        }
        // if obj == node.data return true
        if (((Comparable<E>)obj).compareTo(node.data)==0){
            return true;
        }
        // if obj > node.data go right
        if(((Comparable<E>)obj).compareTo(node.data)>0){
            return contain(obj,node.right);
        }
        // if obj < node.data go left
        return contain(obj,node.left);
    }


    public boolean contain (E obj){
        return contain(obj,root);
    }

    private Node<E> search(E obj,Node<E> node){
        // tree is empty
        if(node == null){
            return null;
        }
        // if obj == node.data return true
        if (((Comparable<E>)obj).compareTo(node.data)==0){
            return node;
        }
        // if obj > node.data go right
        if(((Comparable<E>)obj).compareTo(node.data)>0){
            return search(obj,node.right);
        }
        // if obj < node.data go left
             return search(obj,node.left);

    }

    private Node<E> predecssor(Node<E> node){
        if(node.right !=null){
           return predecssor(node.right);
        }
        return node;
    }

    public E predecssor(E obj){
        Node<E> node = search(obj,root);
       return predecssor(node.left).data;
    }

    public Node<E> getRoot() {
        return root;
    }

}