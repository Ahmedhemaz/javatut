package com.cracking;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;
import java.util.Stack;

public class BFS {
    private boolean[] marked;
    private int[] edgeTo;
    private int[] distTo;
    private int source;

    public BFS(Graph graph, int source){
        this.marked = new boolean[graph.getNumberOfVertices()];
        this.edgeTo = new int[graph.getNumberOfVertices()];
        this.distTo = new int[graph.getNumberOfVertices()];
        this.source = source;
        this.bfs(graph, source);
    }

    private void bfs(Graph graph, int source){
        Deque<Integer> queue = new ArrayDeque<>();
        this.marked[source] = true;
        queue.add(source);
        while (!queue.isEmpty()){
            int vertex = queue.remove();
            for (int v : graph.getAdjacencyList(vertex)) {
                if (!this.marked[v]){
                    this.marked[v] = true;
                    this.edgeTo[v] = vertex;
                    this.distTo[v] = this.distTo[vertex] + 1;
                    queue.add(v);
                }
            }
        }
    }

    private boolean hasPathTo(int vertex){
        return  this.marked[vertex];
    }

    public Iterable<Integer> pathTo(int v){
        if (!hasPathTo(v)){return null;}
        Stack<Integer> path = new Stack<>();
        for (int x = v; x!=this.source; x=this.edgeTo[x]){
            path.push(x);
        }
        path.push(this.source);
        return path;
    }

    public int[] getEdgeTo() {
        return edgeTo;
    }

    public int[] getDistTo() {
        return distTo;
    }
}
