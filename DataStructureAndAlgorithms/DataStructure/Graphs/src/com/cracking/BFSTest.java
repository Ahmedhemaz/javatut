package com.cracking;

public class BFSTest {
    public static void main(String[] args) {
        Graph graph = new Graph(6);
        graph.addEdge(0,5);
        graph.addEdge(0,1);
        graph.addEdge(0,2);
        graph.addEdge(1,2);
        graph.addEdge(2,4);
        graph.addEdge(2,3);
        graph.addEdge(4,3);
        graph.addEdge(3,5);
        BFS bfs = new BFS(graph, 0);
        bfs.pathTo(3).forEach(v -> System.out.println(v));
    }
}
