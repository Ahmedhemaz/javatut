package com.cracking;

import java.util.Stack;

public class DFS <T> {
    private boolean[] marked;
    private int[] edgeTo;
    private final int source;

    public DFS(Graph graph ,int source){
        this.marked = new boolean[graph.getNumberOfVertices()];
        this.edgeTo =  new int[graph.getNumberOfVertices()];
        this.source = source;
        // in case of finding the path to vertex with recursion
        for (int i = 0; i < this.edgeTo.length; i++){
            this.edgeTo[i] = Integer.MAX_VALUE;
        }
        this.dfs(graph, source);
    }

    private void dfs(Graph graph, int v1){
        this.marked[v1] = true;
        for (int v2 : graph.getAdjacencyList(v1)) {
            if (!this.marked[v2]){
                this.edgeTo[v2] = v1;
                this.dfs(graph, v2);
            }
        }
    }

    public boolean hasPathTo(int v){
        return  this.marked[v];
    }

    public Iterable<Integer> pathTo(int v)
    {
        if (!hasPathTo(v)) return null;
        Stack<Integer> path = new Stack<Integer>();
        for (int x = v; x != this.source; x = this.edgeTo[x])
            path.push(x);
        path.push( this.source);
        return path;
    }

    public int findPathTo(int v){
        if (this.edgeTo[v] == Integer.MAX_VALUE){
            return Integer.MAX_VALUE;
        }
        System.out.println(this.edgeTo[v]);
        return findPathTo(this.edgeTo[v]);
    }

    public boolean[] getMarked() {
        return marked;
    }

    public void setMarked(boolean[] marked) {
        this.marked = marked;
    }

    public int[] getEdgeTo() {
        return edgeTo;
    }

    public void setEdgeTo(int[] edgeTo) {
        this.edgeTo = edgeTo;
    }

    public int getSource() {
        return source;
    }
}
