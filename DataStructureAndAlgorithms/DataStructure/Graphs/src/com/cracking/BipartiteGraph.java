package com.cracking;

public class BipartiteGraph {
    private boolean[] marked;
    private boolean[] colored;
    private boolean isBipartite;
    private final int  source;

    public BipartiteGraph(Graph graph, int source){
        this.marked = new boolean[graph.getNumberOfVertices()];
        this.colored = new boolean[graph.getNumberOfVertices()];
        this.isBipartite = true;
        this.source = source;
        this.bfs(graph, source);
    }

    private void bfs(Graph graph, int source){
        this.marked[source] = true;
        for (int vertex : graph.getAdjacencyList(source)) {
            if (!this.marked[vertex]){
                this.colored[vertex] = !this.colored[source];
                this.bfs(graph, vertex);
            }else if (colored[vertex] == colored[source]){
                this.isBipartite = false;
            }
        }
    }

    public boolean isBipartite() {
        return isBipartite;
    }
}
