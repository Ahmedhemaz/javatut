package com.cracking;

import java.util.Iterator;

public class Graph {
    private final int numberOfVertices;
    private int numberOfEdges;
    private Bag<Integer>[] adjacencyList;

    public Graph(int numberOfVertices){
        this.numberOfVertices = numberOfVertices;
        this.adjacencyList = (Bag<Integer>[]) new Bag[numberOfVertices];
        for (int i = 0; i < this.adjacencyList.length; i++){
            this.adjacencyList[i] = new Bag<>();
        }
    }

    public void addEdge(int v1, int v2){
        this.adjacencyList[v1].add(v2);
        this.adjacencyList[v2].add(v1);
        this.numberOfEdges ++;
    }

    public int getNumberOfEdges() {
        return numberOfEdges;
    }

    public int getNumberOfVertices() {
        return numberOfVertices;
    }

    public Bag<Integer> getAdjacencyList(int v){
        return  this.adjacencyList[v];
    }

    public Iterator<Integer> getIterator(int v){
        return this.adjacencyList[v].iterator();
    }
}
