package com.cracking;

public class DFSTest {
    public static void main(String[] args) {
        Graph integerGraph = new Graph(13);
        integerGraph.addEdge(0,1);
        integerGraph.addEdge(0,2);
        integerGraph.addEdge(0,5);
        integerGraph.addEdge(0,6);
        integerGraph.addEdge(6,4);
        integerGraph.addEdge(4,3);
        integerGraph.addEdge(4,5);
        integerGraph.addEdge(5,3);
        DFS<Integer> integerDFS = new DFS<>(integerGraph, 0);
        integerDFS.pathTo(1).forEach(x -> System.out.println(x));
        // path with recursion
//        integerDFS.findPathTo(3);
        BipartiteGraph bipartiteGraph = new BipartiteGraph(integerGraph,0);

    }
}
