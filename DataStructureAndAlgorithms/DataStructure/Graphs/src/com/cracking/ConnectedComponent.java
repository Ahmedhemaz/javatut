package com.cracking;

public class ConnectedComponent {
    private boolean[] marked;
    private int[] ids;
    private int count;

    public ConnectedComponent(Graph graph){
        this.marked = new boolean[graph.getNumberOfVertices()];
        this.ids = new int[graph.getNumberOfVertices()];
        for (int source = 0; source < ids.length; source++){
            if (!this.marked[source]){
                this.dfs(graph, source);
                this.count++;
            }
        }
    }

    private void dfs(Graph graph, int source){
        this.marked[source] = true;
        this.ids[source] = count;
        for (int vertex : graph.getAdjacencyList(source)) {
            if (!this.marked[vertex]){
                this.dfs(graph, vertex);
            }
        }
    }

    public boolean connected(int v, int w) { return ids[v] == ids[w]; }

    public int id(int v) { return ids[v]; }

    public int count() { return count; }


}
