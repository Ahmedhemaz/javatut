package com.cracking;

public class ConnectedComponentTest {
    public static void main(String[] args) {
        Graph integerGraph = new Graph(13);
        integerGraph.addEdge(0,1);
        integerGraph.addEdge(0,2);
        integerGraph.addEdge(0,5);
        integerGraph.addEdge(0,6);
        integerGraph.addEdge(6,4);
        integerGraph.addEdge(4,3);
        integerGraph.addEdge(4,5);
        integerGraph.addEdge(5,3);
        integerGraph.addEdge(7,8);
        integerGraph.addEdge(9,10);
        integerGraph.addEdge(9,12);
        integerGraph.addEdge(9,11);
        integerGraph.addEdge(11,12);
        ConnectedComponent connectedComponent = new ConnectedComponent(integerGraph);
        System.out.println(connectedComponent.connected(0,1));

    }
}
