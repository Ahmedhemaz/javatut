package com.cracking;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Edge<Integer>  edge0_1 = new Edge<>(0,1);
        Edge<Integer>  edge0_2 = new Edge<>(0,2);
        Edge<Integer>  edge0_6 = new Edge<>(0,6);
        Edge<Integer>  edge0_5 = new Edge<>(0,5);
        Edge<Integer>  edge5_3 = new Edge<>(5,3);
        Edge<Integer>  edge5_4 = new Edge<>(5,4);
        Edge<Integer>  edge3_4 = new Edge<>(3,4);
        Edge<Integer>  edge6_4 = new Edge<>(6,4);
        Edge<Integer>  edge7_8 = new Edge<>(7,8);
        Edge<Integer>  edge9_11 = new Edge<>(9,11);
        Edge<Integer>  edge9_10 = new Edge<>(9,10);
        Edge<Integer>  edge9_12 = new Edge<>(9,12);
        Edge<Integer>  edge11_12 = new Edge<>(11,12);

        ArrayList<Edge<Integer>> graph = new ArrayList<>();
        graph.add(edge0_1); graph.add(edge0_2); graph.add(edge0_6); graph.add(edge0_5); graph.add(edge5_3); graph.add(edge5_4);
        graph.add(edge3_4); graph.add(edge6_4); graph.add(edge7_8); graph.add(edge9_11); graph.add(edge9_10); graph.add(edge9_12);
        graph.add(edge11_12);

        for (int i = 0; i < graph.size(); i ++){
            for (Edge<Integer> edge: graph) {
                if (edge.getFrom().equals(graph.get(i).getFrom())){
                    System.out.println(edge.getFrom() + "-" + edge.getTo());
                }
            }
        }

    }
}
