package com.company;

import java.util.Iterator;

public class HashTable<k,v> implements Iterable<k>{
    class HashElement<k,v> implements Comparable<HashElement<k,v>>  {
        private k key;
        private v value;

        public HashElement(k key, v value) {
            this.key = key;
            this.value = value;
        }

        public int compareTo(HashElement<k,v> o) {
            return ((Comparable<k>) this.key).compareTo(key);

        }
    }

    private int tableSize,numElement;
    private double maxLoadFactor,loadFactor;
    private LinkedList<HashElement<k,v>> [] hArray;

    @Override
    public Iterator<k> iterator() {
        return null;
    }

    public HashTable(int tableSize) {
        this.tableSize = tableSize;
        this.numElement=0;
        this.maxLoadFactor=0.75;
        hArray = (LinkedList<HashElement<k,v>>[]) new LinkedList[tableSize];
        for(int i=0;i<tableSize;i++){
            hArray[i] = new LinkedList<HashElement<k,v>>();
        }
    }

    private int arrayindex (k key) {
        int hashValue = key.hashCode();
        hashValue = hashValue & 0x7fffffff;
        hashValue = hashValue % tableSize;
        return hashValue;

    }
    private int arrayindex (k key,int newSize) {
        int hashValue = key.hashCode();
        hashValue = hashValue & 0x7fffffff;
        hashValue = hashValue % newSize;
        return hashValue;

    }



    public boolean add(k key, v value) {
        loadFactor=numElement/tableSize;
        System.out.println(loadFactor);
        if(loadFactor>=maxLoadFactor) {
            resize(tableSize*2);
        }
        HashElement<k,v> hashElement = new HashElement<k, v>(key,value);
        int hashValue = arrayindex(key);
        System.out.println(hashValue);
        hArray[hashValue].addFirst(hashElement);
        numElement++;
        System.out.println(numElement);
        return true;

    }


    public boolean remove(k key){
        int hashValue = arrayindex(key);
        hArray[hashValue].removeLast();
        numElement++;
        return true;


    }


    public v getvalue(k key) {
        int hashValue = arrayindex(key);
        for (HashElement<k,v> hashElement : hArray[hashValue] ) {
            if (((Comparable<k>) key).compareTo(hashElement.key) == 0) {
                return hashElement.value;
            }
        }
        return null;
    }


    public void resize(int newSize) {
         LinkedList<HashElement<k,v>>[] newArray = (LinkedList<HashElement<k,v>>[])new LinkedList[newSize];
         for(int i = 0; i<newSize; i++){
             newArray[i] = new LinkedList<HashElement<k, v>>();
         }
        for ( k key:this) {
             v value = getvalue(key);
             HashElement<k,v> hashElement = new HashElement<k, v>(key,value);
             newArray[arrayindex(key,newSize)].addFirst(hashElement);

        }
        hArray = newArray;
        tableSize = newSize;
    }


}
