package com.company;

import java.util.Iterator;

public class LinkedList<T> implements Iterable<T>{
    @Override
    public Iterator<T> iterator() {
        return null;
    }


    class Node<T> {
        T data;
        Node<T> next;

        public Node(T data) {
            this.data = data;
            this.next = null;
        }
    }



    private Node<T> head;
    private Node<T> tail;
    private int currentSize;

    public LinkedList() {
        this.head = null;
        this.tail = null;
        this.currentSize = 0;
    }

    public void addFirst(T data) {
        Node<T> node = new Node<T>(data);
        if(head==null) {
            head = tail =node;
            node.next = null;
            currentSize++;
            return;
        }
        node.next=head;
        head = node;
        currentSize++;
        return;
    }


    public void addLast(T data) {
        Node<T> node = new Node<T>(data);
        if(head == null) {
            addFirst(data);
        }
        tail.next=node;
        tail=node;
        currentSize++;
        return;
    }


    public T removeFirst() {
        if(head == null) {
            return null ;
        }
        T temp = head.data;
        if(head == tail) {
            head = tail = null;
        }
        head = head.next;
        currentSize--;
        return temp ;
    }



    public T removeLast() {
        Node<T> current = head , previous = null;
        if(head == null) {
            return null;
        }
        if(head==tail){
            return removeFirst();
        }
        while (current.next != null) {
            previous = current;
            current= current.next;
        }
        previous.next=null;
        tail=previous;
        currentSize--;
        return current.data;
    }

    public T remove(T data) {
        Node<T> current = head ,previous = null;

        while (current !=null) {
            if(((Comparable<T>)current.data).compareTo(data)==0) {
                if(head == null){
                    return null;
                }
                if (current == head){
                    return removeFirst();
                }
                if(current == tail) {
                    return removeLast();
                }

                previous.next=current.next;
                currentSize--;
                return current.data;

            }
                previous = current;
                current = current.next;

        }
        return null;
    }

    public boolean contains(T data) {
        Node<T> current = head;
        while (current != null) {
            if (((Comparable<T>) current.data).compareTo(data) == 0) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    public T peekFirst(T data) {
        if(head == null)
            return null;
        return head.data;
    }

    public T peekLast(T data) {
        if(head == null)
            return null;
        return tail.data;
    }

    public void printLinkedList () {
        Node<T> current = head;
        System.out.print("Head");
        for(int i = 0; i<currentSize;i++) {
            System.out.print("->");
            System.out.print(current.data);
            current = current.next;

        }
    }

}
