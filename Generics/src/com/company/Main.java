package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Footballplayer joe = new Footballplayer("joe");
        Baseballplayer pat = new Baseballplayer("pat");
        Soccerplayer messi = new Soccerplayer("messi");

        Team<Footballplayer> adelaideCrows = new Team<>("Adelaide Crows");
        adelaideCrows.addPlayer(joe);

        System.out.println(adelaideCrows.numPlayers());



        Team<Baseballplayer> baseballTeam = new Team<>("Chicago cubs");
        baseballTeam.addPlayer(pat);

//        Team<String> brokenTeam = new Team<>("this won't work");
//        brokenTeam.addPlayer("no-one");

        System.out.println(baseballTeam.numPlayers());


        Team<Footballplayer> melbourne = new Team<>("Melbourne");
        Footballplayer banks = new Footballplayer("Gordon");
        melbourne.addPlayer(banks);

        Team<Footballplayer> hawthorn = new Team<>("Hawthorn");
        Team<Footballplayer> fremantle = new Team<>("Fremantle");



        hawthorn.matchResult(fremantle,1,0);
        hawthorn.matchResult(adelaideCrows,3,8);
        adelaideCrows.matchResult(fremantle,2,1);


        System.out.println("Rankings");
        System.out.println(adelaideCrows.getTeamName() + ": " + adelaideCrows.ranking());
        System.out.println(melbourne.getTeamName() + ": " + melbourne.ranking());
        System.out.println(fremantle.getTeamName() + ": " + fremantle.ranking());
        System.out.println(hawthorn.getTeamName() + ": " + hawthorn.ranking());


        System.out.println(adelaideCrows.compareTo(melbourne));
        System.out.println(adelaideCrows.compareTo(hawthorn));
        System.out.println(hawthorn.compareTo(adelaideCrows));
        System.out.println(melbourne.compareTo(adelaideCrows));

    }
}