package com.company;

import java.util.ArrayList;

public class Team<T extends Player> implements Comparable<Team<T>> {

    private String teamName;

    private int played = 0;
    private int won = 0;
    private int lost = 0;
    private int tied = 0;



    private ArrayList<T> members = new ArrayList<>();

    public Team(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public boolean addPlayer (T player) {
        if(members.contains(player)) {
            /*
            without T extends player we need to cast player object to player
            class because the compiler will not know what type will be their
            ((Player)player).getName();
            *Not recommended*
            */
            System.out.println(player.getName() + " is already on this team.");
            return false;
        } else {
            members.add(player);
            System.out.println(player.getName() + " picked for team" + this.teamName);
            return true;

        }
    }


    public int numPlayers() {
        return this.members.size();
    }

    public void matchResult(Team<T> opponent, int ourScore, int thierScore) {
        if(ourScore > thierScore) {
            won++;
        } else if (ourScore == thierScore) {
            tied ++;
        } else {
            lost ++;
        }
        played++;
        if(opponent !=null) {
            opponent.matchResult(null,thierScore,ourScore);
        }
    }

    public int ranking () {
        return (won*2) + tied;
    }


    @Override
    public int compareTo(Team<T> team) {
        if(this.ranking() > team.ranking())
            return 1;
        else if(this.ranking() < team.ranking())
            return -1;
        else
            return 0;
    }
}