package com.company;

public class Customer {

    private String name;
    private int transaction;

    public Customer(String name, int transaction) {
        this.name = name;
        this.transaction = transaction;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTransaction() {
        return transaction;
    }

    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }

    public
}
