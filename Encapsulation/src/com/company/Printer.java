package com.company;

public class Printer {

    private int tonerLevel;
    private int blankPaperNumber;
    private int pagesPrinting;
    private boolean duplex;

    public Printer(int tonerLevel, int papersNumber, boolean duplex) {
        if (tonerLevel<=100 && tonerLevel>0) {
            this.tonerLevel = tonerLevel;
        } else {
            System.out.println("enter a valid toner level");
        }
        this.blankPaperNumber = papersNumber;
        this.duplex = duplex;
    }


    public   void fillUpToner () {
        if(this.tonerLevel<100 && this.tonerLevel>=0){
            while (this.tonerLevel<100) {
                this.tonerLevel ++;
            }
        } else {
            System.out.println("toner level is full ya 3aaaam");
        }
    }


    public void printing (int numberOfPrintingPages) {
        this.pagesPrinting += numberOfPrintingPages;
        this.blankPaperNumber -= numberOfPrintingPages;
        System.out.println("Paper printed are "+this.pagesPrinting);
        System.out.println("remaining blank papers are " + this.blankPaperNumber);
    }




    public int getTonerLevel() {
        return tonerLevel;
    }

    public int getPapersNumber() {
        return blankPaperNumber;
    }

    public int getPagesPrinting() {
        return pagesPrinting;
    }

    public boolean isDuplex() {
        return duplex;
    }
}
