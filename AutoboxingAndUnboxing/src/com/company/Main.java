package com.company;


import java.util.ArrayList;

class IntClass {
    private int myValue;

    public IntClass(int myValue) {
        this.myValue = myValue;
    }

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue() {
        this.myValue = myValue;
    }

}


public class Main {

    public static void main(String[] args) {

        String[] strArray = new String[10];
        int[] intArray = new int[10];


        ArrayList<String> strArrayList = new ArrayList<String>();
        strArrayList.add("Ahmed");

        ArrayList<IntClass> intClassArrayList = new ArrayList<IntClass>();
        intClassArrayList.add(new IntClass(45));


        Integer intValue = new Integer(54);
        Double doubleValue = new Double(12.25);

        ArrayList<Integer> integerArrayList = new ArrayList<Integer>();
        for(int i=0;i<=10;i++) {
            integerArrayList.add(Integer.valueOf(i));
        }

        for(int i=0;i<=10;i++) {
            System.out.println(i+"---->"+integerArrayList.get(i).intValue());
        }


        Integer myIntValue = 56; // = Integer.valueOf(56); when compile
        int myInt = myIntValue; // myIntValue.intValue(); when compile

    }
}
